<!-- MARKER: do not edit this section directly. Edit services/service-catalog.yml then run scripts/generate-docs -->
#  Waf Service
* [Service Overview](https://dashboards.gitlab.net/d/waf-main)
* **Alerts**: https://alerts.gitlab.net/#/alerts?filter=%7Btype%3D%22waf%22%2C%20tier%3D%22lb%22%7D
* **Label**: gitlab-com/gl-infra/production~"Service:WAF"

## Logging

* []()

## Troubleshooting Pointers

* [../cloudflare/README.md](../cloudflare/README.md)
* [../cloudflare/managing-traffic.md](../cloudflare/managing-traffic.md)
* [../cloudflare/oncall.md](../cloudflare/oncall.md)
* [../cloudflare/terraform.md](../cloudflare/terraform.md)
* [../release.gitlab.net/README.md](../release.gitlab.net/README.md)
<!-- END_MARKER -->
